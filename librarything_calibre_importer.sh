#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

command -v calibredb > /dev/null || exit 1
command -v jq > /dev/null || exit 1

FILE=${1:?"No file specified!"}

if [ "$(dirname "$FILE")" == "." ]; then
  echo "Please specify the full path to file: '$FILE'"
  exit 1
fi

cleanup() {
  [ -d "$MYTEMPPATH" ] && rm -rxf -- "$MYTEMPPATH"
  echo "[DONE]"
}

trap cleanup EXIT

MYTEMPPATH="${TMPDIR}/$(basename -s .sh "$0")"
mkdir -p "$MYTEMPPATH"
DIR=$(mktemp -d "${MYTEMPPATH}/calibre_importer.XXXX")

cd "$DIR" || exit 1

while read -r line
do
  TMPFILE=$(mktemp XXXX) || exit 1
  cat > "$TMPFILE" <<< "$line"
  TITLE=$(jq -crM ".title" "$TMPFILE")
  ISBN=$(jq -crM '.originalisbn' "$TMPFILE" | sed 's/null//' )
  AUTHOR=$(jq -crM '.primaryauthor' "$TMPFILE")

  printf " -- \"$TITLE\" "
  calibredb --with-library "${CALIBRE_LIBRARY_URL}" \
            add --empty --tags "${CALIBRE_TAGS_TO_ADD}" \
            --isbn "$ISBN" \
            --title "$TITLE" \
            --authors "$AUTHOR"
  rm -- "$TMPFILE"
done <<<"$(jq -crM '.[] | {title, primaryauthor, originalisbn}' "$FILE")"
